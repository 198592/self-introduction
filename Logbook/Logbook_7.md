# MY ENGINEERING LOGBOOK

|Logbook 08: Week 8| Date: 17 December 2021|
|------|------|
<div align="center">
_________________________________________________________________________
</div>


|Agenda And Goals|
|------------|

- The deposit for the tent had to be within this week
- Got the update from the RSK on the possible week of getting the tent which is within Week 11 or Week 12

|Decision in Solving The Problem|
|------------|

- Always keep up to date on whether Fikri and Dr Salah on the deposit payment
- Text RSK after we did the deposit payment to make sure that they were aware and could process our order.

|Method Solving The Problem|
|------------|

- By always get in touch with RSK Companny.

|Justification in Solving The Problem|
|------------|

- To make sure that we get the tent when we want to fly the aircraft

|The Impact of The Decision|
|------------|

- When we do the deposit, the supplier can proceed with the fabrication process of the tent.

|The Next Step|
|------------|

- Update to the supplier about the payment
- Make sure that the tent is going to arrive by the time that we do the assemblymen of the air hybrid.
