## MY ENGINEERING LOGBOOK

|Logbook 02: Week 2| Date: 29 October 2021|
|------|------|



<div align="center">
_________________________________________________________________________
</div>


|Agenda And Goals|
|------------|


- The agenda of this week is to discuss the project costing and the milestone of the project throughout this semester. We aim to do the first flying testing on the air hybrid UAV in Week 7/8. The first flying testing is without the payload and the second flying testing which is with the payload will be done in Week 13/14. We also learned how to use Gitlab from Mr. Azizi as we need to make reports/logbooks via Gitlab.

|Decision in Solving The Problem|
|------------|


- As to stick to our goals, we did the Gantt chart which can be accessed by any subsystem, to update the expected date to achieve the milestone of the project. Thus, we are able to stay on track in finishing the project according to our expectations. 


|Method To Solve The Problem|
|------------|


- We discussed the problems on Discord in order to achieve the solutions that were agreed upon by all.

|Justification in Solving The Problem|
|------------|

- We made the decision based on our discussion and we also consulted advice from Dr Salahuddin and Fikri

|The Impact of The Decision|
|------------|



- By doing this decision, we were able to plan ahead on the schedule of the project. We also had a purpose or milestone to achieve as time goes on. The other subsystems will also be aware of the timeline for our group.

|The Next Step|
|------------|

- As for the next step, we will be presenting our project milestones/timelines, so that the other subsystems are aware of the works that we will do throughout this semester. We also will observe the other subsystem’s work to make sure that the project will work smoothly.
