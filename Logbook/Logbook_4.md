# MY ENGINEERING LOGBOOK

|Logbook 05: Week 5 | Date: 19 November 2021|
|------|------|



<div align="center">
_________________________________________________________________________
</div>


|Agenda And Goals|
|------------|


- Calculate the parameter related to the construction of the air hybrid.

- Update the calculation in the Google spreadsheet.

- Deciding on the type of tent to use to store the air balloon that filled with the helium later on.

- Meeting with Fikri to update on tent matters.

|Method To Solve The Problem|
|------------|

- Created the spreadsheet to do the calculation.

|Decision in Solving The Problem|
|------------|

- We made this decision as we considered that all students must need to know these parameters.

|Justification in Solving The Problem|
|------------|

- By creating the spreadsheet, everyone can access to know the parameters of the air hybrid

|The Impact of The Decision|
|------------|

- Everyone can access and know the parameters

|The Next Step|
|------------|
- Make sure that all parameters that we need
- Make sure that the type of tent has been decided.

