# MY ENGINEERING LOGBOOK

|Logbook 12: Week 12| Date: 10 - 14 January 2022|
|------|------|
<div align="center">
_________________________________________________________________________
</div>

|Agenda And Goals|
|------------|

- To support each of the tent pole with the materials at the hangar.
- Make sure the support is strong enoung.
- Contact another two company for helium gas which is Asia Pasific and GasWorld.


|Decision in Solving The Problem|
|------------|

- By asking Dr Salah whether we can use the materials in hangar or not
- The helium gas companies were suggested by Dr.

|Method Solving The Problem|
|------------|

- By having the meeting to ask all the student who is free at that time to ask the FSI team.
- By searching the contact number at the official website and call/text them.

|Justification in Solving The Problem|
|------------|

- To avoid the catastrophic happen as the supplier said the tent might be flying.
- To survey the most possible company that can supply the helium gas for this project.

|The Impact of The Decision|
|------------|

- We able to support each tent pole.

<img src="https://gitlab.com/198592/self-introduction/uploads/6df401e5b2c7b0e3ac7a55557d638739/WhatsApp_Image_2022-01-14_at_3.24.04_PM.jpeg" width=300 align=middle>

<img src="https://gitlab.com/198592/self-introduction/uploads/17ab27a69bc61cb903f9f7ce7c76daec/WhatsApp_Image_2022-01-14_at_3.24.04_PM__1_.jpeg" width=300 align=middle>

<img src="https://gitlab.com/198592/self-introduction/uploads/79780c6a4dc8fdaa01810c01b7652634/WhatsApp_Image_2022-01-14_at_3.24.04_PM__2_.jpeg" width=300 align=middle>

<img src="https://gitlab.com/198592/self-introduction/uploads/51cd1ed03fbde27138a962294952ada0/WhatsApp_Image_2022-01-14_at_3.24.04_PM__3_.jpeg" width=300 align=middle>

|The Next Step|
|------------|

- Make sure the registration and the deposit for helium gas has been done.
- Make sure that we got the helium gas before 21 January 2022 (Friday)
