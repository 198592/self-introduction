# Self introduction

### Biography

<img src="https://gitlab.com/198592/self-introduction/uploads/5bc843242598f94a6801e13761750122/photo_2021-10-29_00-48-17.jpg" width=300 align=middle>

* *NAME:* NUR ADLINA BINTI MOHD NAZRI

* *AGE:* 22

* *MATRIC NO:* 198592

* *COURSE:* Bachelor Of Aerospace Engineering  With Honors

## Strength & Weakness

| Strength | Weakness |
| :----------: | :-----------: |
| persistent | impulsive |
| confident | overconfident | 
| adaptive | introvert |
